# A handbook for configuration Linux

## Packages

```bash
sudo pacman -S base-devel yay
sudo pacman -S tar unrar unzip
sudo pacman -S wget curl git
sudo pacman -S tmux htop neofetch
sudo pacman -S nano vim
sudo pacman -S cron net-tools
sudo pacman -S nodejs npm yarn
```

## Apps

```bash
yay -S skypeforlinux-stable-bin
yay -S visual-studio-code-bin
yay -S google-chrome
yay -S anydesk-bin
yay -S filezilla
yay -S postman
yay -S vlc
```

### Docker
```bash
sudo pacman -S docker docker-compose
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
```

## Config

```bash
sudo sysctl -w fs.inotify.max_user_instances=1024
sudo sysctl -w fs.inotify.max_user_watches=524288
```
